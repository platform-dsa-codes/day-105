//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

class GFG {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
            new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while (t-- > 0) {
            
            String input[] = read.readLine().split(" ");
            String a = input[0];
            String b = input[1];
            Solution ob = new Solution();
            System.out.println(ob.addBinary(a,b));
        }
    }
}
// } Driver Code Ends


class Solution {
    String addBinary(String A, String B) {
        StringBuilder result = new StringBuilder();
        int carry = 0;
        int maxLength = Math.max(A.length(), B.length());
        
        for (int i = 0; i < maxLength || carry != 0; i++) {
            int digitA = (i < A.length()) ? A.charAt(A.length() - 1 - i) - '0' : 0;
            int digitB = (i < B.length()) ? B.charAt(B.length() - 1 - i) - '0' : 0;
            int sum = digitA + digitB + carry;
            result.append((char)('0' + sum % 2));
            carry = sum / 2;
        }

        // Reverse the result
        result.reverse();
        
        // Trim leading zeros if any
        while (result.length() > 1 && result.charAt(0) == '0') {
            result.deleteCharAt(0);
        }
        
        return result.toString();
    }
}
